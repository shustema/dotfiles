" --------------------------------------------------
" Get Plugged
" --------------------------------------------------
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" --------------------------------------------------
"  Basic Preferences
" --------------------------------------------------
highlight Visual cterm=reverse ctermbg=NONE
runtime ftplugin/man.vim
let $PAGER=''

set nocompatible
set encoding=utf-8
set backspace=indent,eol,start
set autoindent
set shiftwidth=2
set softtabstop=2
set expandtab
set nobackup
set history=50
set ruler
set number relativenumber
set showcmd
set notimeout ttimeout ttimeoutlen=200
set hidden
set wildmenu
set ignorecase
set smartcase
set nostartofline
set laststatus=2
set confirm
set visualbell
set t_vb=
set mouse=a

" --------------------------------------------------
"  Window Split
" --------------------------------------------------
" Remap redraw command to avoid collisions
nnoremap <C-L> :redraw!<CR>
nnoremap <C-I> :nohl<CR><C-L>

" Map split window navigation
noremap <C-J> <C-W><C-J>
noremap <C-K> <C-W><C-K>
noremap <C-L> <C-W><C-L>
noremap <C-H> <C-W><C-H>

" Map window maximize/minimize commands. Store all windows in buffer to be
" recovered on maximizing again
nnoremap <C-W>O :call MaximizeToggle()<CR>
nnoremap <C-W>o :call MaximizeToggle()<CR>
nnoremap <C-W><C-O> :call MaximizeToggle()<CR>
nnoremap <leader>mm :call MaximizeToggle()<CR>

function! MaximizeToggle()
  if exists("s:maximize_session")
    exec "source " . s:maximize_session
    call delete(s:maximize_session)
    unlet s:maximize_session
    let &hidden=s:maximize_hidden_save
    unlet s:maximize_hidden_save
  else
    let s:maximize_hidden_save = &hidden
    let s:maximize_session = tempname()
    set hidden
    exec "mksession! " . s:maximize_session
    only
  endif
endfunction

set splitbelow
set splitright

" --------------------------------------------------
"  Call Bracket Pairs
" --------------------------------------------------
function! s:CloseBracket()
  let line = getline('.')
  if line =~# '^\s*\(struct\|class\|enum\) '
    return "{\<Enter>};\<Esc>0"
  elseif searchpair('(', '', ')', 'bmn', '', line('.'))
    return "{\<Enter>});\<Esc>0"
  else
    return "{\<Enter>}\<Esc>0"
  endif
endfunction
" inoremap <expr> {<Enter> <SID>CloseBracket()

" --------------------------------------------------
"  Call plugins
" --------------------------------------------------
call plug#begin('~/.vim/plugged')
  " status bar
  Plug 'vim-airline/vim-airline'
  " nerdtree
  " Plug 'preservim/nerdtree'
  " easymotion
  Plug 'easymotion/vim-easymotion'
  if v:version >= 800
    " gutentags
    Plug 'ludovicchabant/vim-gutentags'
    " coc
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
  endif

  "Plug 'junegunn/fzf'
  "Plug 'junegunn/fzf.vim'
  "Plug 'pbogut/fzf-mru.vim'

  "Plug 'SirVer/ultisnips'
  "Plug 'honza/vim-snippets'
  "Plug 'aklt/plantuml-syntax'
  "Plug 'hrp/EnhancedCommentify'
  Plug 'tomtom/tcomment_vim'
call plug#end()

" --------------------------------------------------
"  Airline
" --------------------------------------------------
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1

" --------------------------------------------------
"  NerdTree
" --------------------------------------------------
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
"autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') | 
"      \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif
"autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | 
"      \ quit | endif
"
"map <C-n> :NERDTreeToggle<CR> 
nmap <C-n> :CocCommand explorer<CR>

" --------------------------------------------------
" EasyMotion
" --------------------------------------------------
let g:EasyMotion_do_mapping = 0 " Disable default mappings 
let g:EasyMotion_smartcase = 1 " Turn on case-insensitive feature

" highlight colors
hi link EasyMotionTarget Search
hi link EasyMotionTarget2First Search
hi link EasyMotionTarget2Second Search
hi link EasyMotionShade Comment

" Jump to anywhere with key
nmap s <Plug>(easymotion-overwin-f)
nmap s <Plug>(easymotion-overwin-f2)

" Move to line j / k 
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)

" --------------------------------------------------
" Gutentags
" --------------------------------------------------
if v:version >= 800
  let g:gutentags_project_root = ['compile_commands.json']
  let g:gutentags_add_default_project_roots = 0
  let g:gutentags_add_ctrlp_root_markers = 0
  let g:gutentags_ctags_tagfile = '.tags'
  let g:gutentags_ctags_extra_args = ['--fields=+niazS', '--extra=+q', '--c++-kinds=+pxI', '--c-kinds=+px']
  let g:gutentags_ctags_exclude = [
        \ '*.git', '*.svg', '*.hg',
        \ 'run/*',
        \ 'objs/*',
        \ 'build',
        \ 'bak',
        \ '*sites/*/files/*',
        \ 'bin',
        \ 'node_modules',
        \ 'bower_components',
        \ 'cache',
        \ 'compiled',
        \ 'doc',
        \ 'example',
        \ 'bundle',
        \ 'vendor',
        \ '*.md',
        \ '*-lock.json',
        \ '*.lock',
        \ '*bundle*.js',
        \ '*build*.js',
        \ '.*rc*',
        \ '*.json',
        \ '*.min.*',
        \ '*.map',
        \ '*.bak',
        \ '*.zip',
        \ '*.pyc',
        \ '*.class',
        \ '*.sln',
        \ '*.Master',
        \ '*.csproj',
        \ '*.tmp',
        \ '*.csproj.user',
        \ '*.cache',
        \ '*.pdb',
        \ 'tags*',
        \ 'cscope.*',
        \ '*.css',
        \ '*.less',
        \ '*.scss',
        \ '*.exe', '*.dll',
        \ '*.o', '*.obj',
        \ '*.lib', '*.so', '*.bin', '*.a',
        \ '*.mp3', '*.ogg', '*.flac',
        \ '*.swp', '*.swo',
        \ '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png',
        \ '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2',
        \ '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx',
        \ ]
endif

" --------------------------------------------------
" CoC
" --------------------------------------------------
let g:coc_node_path = '/global/freeware/Linux/RHEL7/node-v10.15.3-linux-x64/bin/node'

let g:coc_global_extensions = [
  \ 'coc-snippets', 
  \ 'coc-pairs', 
  \ 'coc-tsserver', 
  \ 'coc-eslint', 
  \ 'coc-prettier', 
  \ 'coc-json', 
  \ 'coc-clangd',
  \ 'coc-explorer',
  \ ]

if v:version >= 800
  " Always show the signcolumn, otherwise it would shift the text each time
  " diagnostics appear/become resolved 
  if has('nvim-0.5.0') || has('patch-8.1.1564')
    set signcolumn=number
  elseif has('patch-7.4.2201')
    set signcolumn=yes
  endif
  
  " Use tab for trigger completion with characters ahead and navigate
  " Use command ':verbose imap <tab>' to make sure tab is not mapped by
  " another plugin

  function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~ '\s'
  endfunction

  inoremap <silent><expr> <Tab>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<Tab>" :
    \ coc#refresh()
  inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

  " Use <c-space> to trigger completion.
  if has('nvim')
    inoremap <silent><expr> <c-space> coc#refresh()
  else
    inoremap <silent><expr> <c-@> coc#refresh()
  endif
  
  " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
  " position. Coc only does snippet and additional edit on confirm.
  " <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
  if exists('*complete_info')
    inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
  else
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  endif
  
  " Use `[g` and `]g` to navigate diagnostics
  " Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
  nmap <silent> [g <Plug>(coc-diagnostic-prev)
  nmap <silent> ]g <Plug>(coc-diagnostic-next)
  
  " GoTo code navigation.
  nmap <silent> gy <Plug>(coc-type-definition)
  nmap <silent> gd <Plug>(coc-definition)
  nmap <silent> gt :call CocAction('jumpDefinition', 'tabe')<CR>
  nmap <silent> gs :call CocAction('jumpDefinition', 'split')<CR>
  nmap <silent> gE :call CocAction('jumpDefinition', 'vsplit')<CR>
  nmap <silent> gi <Plug>(coc-implementation)
  nmap <silent> gr <Plug>(coc-references)
 
  " Use K to show documentation in preview window.
  nnoremap <silent> K :call <SID>show_documentation()<CR>
  
  function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
      execute 'h '.expand('<cword>')
    elseif (coc#rpc#ready())
      call CocAction('doHover')
    else
      execute '!' . &keywordprg . " " . expand('<cword>')
    endif
  endfunction
  
  " Highlight the symbol and its references when holding the cursor.
  autocmd CursorHold * silent call CocActionAsync('highlight')
  
  " Symbol renaming.
  nmap <leader>rn <Plug>(coc-rename)
  
  " Formatting selected code.
  xmap <leader>f  <Plug>(coc-format-selected)
  nmap <leader>f  <Plug>(coc-format-selected)
  
  augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder.
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
  augroup end
  
  " Applying codeAction to the selected region.
  " Example: `<leader>aap` for current paragraph
  xmap <leader>a  <Plug>(coc-codeaction-selected)
  nmap <leader>a  <Plug>(coc-codeaction-selected)
  
  " Remap keys for applying codeAction to the current buffer.
  nmap <leader>ac  <Plug>(coc-codeaction)
  " Apply AutoFix to problem on the current line.
  nmap <leader>qf  <Plug>(coc-fix-current)
  
  xmap if <Plug>(coc-funcobj-i)
  omap if <Plug>(coc-funcobj-i)
  xmap af <Plug>(coc-funcobj-a)
  omap af <Plug>(coc-funcobj-a)
  xmap ic <Plug>(coc-classobj-i)
  omap ic <Plug>(coc-classobj-i)
  xmap ac <Plug>(coc-classobj-a)
  omap ac <Plug>(coc-classobj-a)
  
  " Add `:Format` command to format current buffer.
  command! -nargs=0 Format :call CocAction('format')
  
  " Add `:Fold` command to fold current buffer.
  command! -nargs=? Fold :call     CocAction('fold', <f-args>)
  
  " Add `:OR` command for organize imports of the current buffer.
  command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')
  
  " Add (Neo)Vim's native statusline support.
  " NOTE: Please see `:h coc-status` for integrations with external plugins that
  " provide custom statusline: lightline.vim, vim-airline.
  "set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
  highlight Pmenu ctermbg=gray guibg=gray
  let g:lightline = {
        \ 'colorscheme' : 'powerlineish',
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'cocstatus', 'readonly', 'filename', 'modified' ] ]
        \ },
        \ 'component_function': {
        \   'cocstatus': 'coc#status'
        \ },
        \ }
  
  "      \ 'colorscheme' : 'materia',
  "      \ 'colorscheme' : 'powerlineish',
  "      \ 'colorscheme' : 'Tomorrow_Night_Blue',
  
  " disable transparent cursor
  let g:coc_disable_transparent_cursor = 1
  
  " Use auocmd to force lightline update.
  autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()
  
  " Mappings for CoCList
  " Show all diagnostics.
  nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
  " Manage extensions.
  nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
  " Show commands.
  nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
  " Find symbol of current document.
  nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
  " Search workspace symbols.
  nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
endif

" --------------------------------------------------
" Termdebug
" --------------------------------------------------
" function! MyTermGdb()
"   set scrolloff=20
"   autocmd! CursorLine
"   packadd termdebug
"   tnoremap <Esc> <C-W>N
"   noremap <silent> <space>gu :call term_sendkeys(4, 'up' . "\n")<CR>
"   noremap <silent> <space>gd :call term_sendkeys(4, 'down' . "\n")<CR>
"   noremap <silent> <space>gl :call term_sendkeys(4, 'adv ' . line('.') . "\n")<CR> 
"   noremap <silent> <space>gt :call term_sendkeys(4, 'tbreak ' . line('.') . "\n")<CR> 
"   noremap <silent> <space>gc :Continue<CR> 
"   noremap <silent> <space>gb :Break<CR> 
"   noremap <silent> <space>gx :Delete<CR> 
"   noremap <silent> <space>gs :Step<CR> 
"   noremap <silent> <space>gn :Over<CR> 
"   noremap <silent> <space>gf :Finish<CR> 
"   noremap <silent> <space>gS :Stop<CR> 
"   noremap <silent> <Leader>q :qa!<CR>
"   noremap q a
"   CocDisable
"   Termdebug
"   wincmd J
"   wincmd k
"   wincmd H
"   wincmd l
"   wincmd j
"   wincmd J
"   call term_sendkeys(4, 'so gdb' . "\n") 
"   Gdb
" endfunction
" 
" if !has('nvim')
"   noremap <silent> <space>gg :call MyTermGdb()<CR>
" endif
